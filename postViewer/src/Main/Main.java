package Main;
import Token.Tokenizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;

public class Main {
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Queue<String> qt = new ArrayDeque<String>();
		try{
			File file = new File("Level1.ps");
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			Tokenizer tknzer = new Tokenizer(qt);
			
			qt = tknzer.tokenize(bufferedReader);
			
			for(String str : qt)
				System.out.println("queue : " + str);
			
			fileReader.close();
		}catch(FileNotFoundException e){
			System.out.println(e);
		}
	}

}
