package Token;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;

public class Tokenizer implements ITokenizer{
	private String tokenStr;
	private Queue<String> qTkn;

	/**
	 * コンストラクタ(引数なし)
	 */
	public Tokenizer() {}
	/**
	 * コンストラクタ（文字列を引数として受け取る）
	 */
	public Tokenizer(String token) {
		// TODO Auto-generated constructor stub
		this.tokenStr = token;
	}
	/**
	 * コンストラクタ(キューを引数として受け取る)
	 */
	public Tokenizer(Queue<String> qToken) {
		// TODO Auto-generated constructor stub
		this.qTkn = qToken;
	}
	/**
	 * 文字列入力ストリームを受け取って分割したトークンを格納したキューを返します。
	 * @param BufferedReader flread 文字列入力ストリーム
	 * @return Queue<String> qToken 分割されたトークン
	 * @throws IOException
	 */
	public Queue<String> tokenize(BufferedReader flread) throws IOException{
		String inputLine;
		String splitInsertStr;
		String[] tkn;
		for(int i=0; (inputLine = flread.readLine()) != null; i++){
			if(inputLine.charAt(0) == '%')
				commentTokenize(inputLine.split("%"));
			else{
				splitInsertStr = insertSpaceSymbol(inputLine);
				tkn = splitInsertStr.split(" ");
				insertFromStringToQueue(tkn);
			}
		}
		return qTkn;	
	}
	/**
	 * トークンに分割した文字列配列からキューに格納します。
	 * @param String[] tkn " "で分割された文字列配列
	 */
	public void insertFromStringToQueue(String[] tkn){
		
		for(int i=0; i<tkn.length; i++){
			if(!tkn[i].equals(""))
				qTkn.add(tkn[i]);
		}
	}
	/**
	 * 文字列配列を受け取りコメントをトークンに分割します。
	 * ※必要なくなりましたが今後のため置きます。
	 * ※ご了承ください。
	 * @param String[] tkn "%"で分割された文字列配列
	 */
	public void commentTokenize(String[] tkn){
		
		for(int i=0; i<tkn.length; i++){
			//System.out.println(i + " : " + tkn[i]);
			if(i == 0){
				qTkn.add("%");
				continue;
			}
			else if(i == 1)
				continue;
			else
				;
			qTkn.add("%" + tkn[i]);
		}
	}
	/**
	 * 記号の前後に空白文字を挿入します。
	 * @param String str 対象の文字列
	 */
	public String insertSpaceSymbol(String str){
		ArrayList<Integer> array = new ArrayList<Integer>();
		StringBuilder sb = new StringBuilder();
		int pre; // 対象の前の位置
		int post; // 対象の後の位置
		int slip=1; // ずれを保証するための変数;
		//記号の位置番号をリストに挿入する
		for(int i=0; i<str.length(); i++){
			if(str.charAt(i) == '/' || str.charAt(i) == '{' || str.charAt(i) == '}' || str.charAt(i) == '%'){
				array.add(i);
			}
			else if(str.charAt(i) == '(' || str.charAt(i) == ')' || str.charAt(i) == '[' || str.charAt(i) == ']'){
				array.add(i);
			}
		}
		
		//挿入のためにStringBufferを使用
		sb.append(str);
		
		for(int i : array){
			//先頭は後の位置だけ空白挿入
			if(i == 0){
				post = i+1;
				sb.insert(post, " ");
				slip++;//位置を動かす
			}
			//他は前後を空白挿入
			else{
				i += slip;
				pre = i-1;
				post = i+1;
				sb.insert(pre, " ");
				sb.insert(post, " ");
				slip += 2;
			}
		}
		return sb.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "tokenStr = " + tokenStr;
	}
}
