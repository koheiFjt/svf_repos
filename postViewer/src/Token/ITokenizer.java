/**
 * 
 */
package Token;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Queue;

/**
 * @author fujita.k
 *
 */
public interface ITokenizer {
	Queue<String> tokenize(BufferedReader file) throws IOException;
}
